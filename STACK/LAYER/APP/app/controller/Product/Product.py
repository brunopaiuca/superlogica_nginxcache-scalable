# coding=utf-8
from flask import request
from flask.views import MethodView
from model.Product.Product import Product


class Root(MethodView):
    def get(self):
        """
        get
        ---
        tags:
            - Banner
        """

        return "Flask Dockerized"


class ProductController(MethodView):
    def get(self):
        """
        get
        ---
        tag:
           - Products
        """

        try:
            product = Product()

            product_list = product.getProductList()
            return product_list

        except:
            msg = "ERRO - ProductController"
            return msg, 500



    def post(self):
        """
        post
        ---
        tag:
           - Products
        """
        try:

            req = request.get_json()
            product = Product()

            ret = product.createProduct( req['Product_Name'], req['Product_Price'] )

            if ret['success'] == "true":
                return ret
            else:
                return ret, 500

        except:
            msg = "POST - ProductController"
            return msg, 500


class ProductByFilterController(MethodView):
    def get(self, filterid):
        """
        get
        ---
        tag:
           - Products By Filter
        """

        #try:
        product = Product()

        product_list_byfilter = product.getProductListByFilter(filterid)
        return product_list_byfilter

        #except:
        #    msg = "ERRO - ProductByFilterController"
        #    return msg, 500


class ProductInfoController(MethodView):
    def get(self, product_id):
        """
        get
        ---
        tag:
           - Products
        """

        try:
            product = Product()

            product_info = product.getProductInfo(product_id)
            return product_info

        except:
            msg = "ERRO - ProductInfoController"
            return msg, 500

    def put(self, product_id):
        """
        put
        ---
        tag:
           - Products
        """

        try:
            req = request.get_json()

            product = Product()
            ret = product.updateProductInfo( product_id,req['Product_Name'], float(req['Product_Price']) )

            if ret['success'] == True:
                return ret 
            return ret, 500

        except:
            msg = "ERRO - PUT ProductInfoController"
            return msg, 500
