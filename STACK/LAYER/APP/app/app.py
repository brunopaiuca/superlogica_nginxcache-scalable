from __init__ import app, api
from flask import jsonify, render_template
from controller.Product.Product import Root, ProductController, ProductByFilterController, ProductInfoController


api.add_resource(Root, '/')

api.add_resource(ProductController, '/products')

api.add_resource(ProductByFilterController, '/products/filter/<int:filterid>')
api.add_resource(ProductInfoController, '/products/<int:product_id>')



#@app.route('/products/filter/<string:whatfilter>', methods=['GET'])
#def GetListProductsByFilter(whatfilter):
#    return 'Get List products By Flter'
#
#
#@app.route('/products/<int:prodcode>', methods=['GET'])
#def GetProductsInfo(prodcode):
#    return 'Get Products Info'
#
#
#@app.route('/products/<int:prodcode>', methods=['PUT'])
#def UpdateProductsInfo(prodcode):
#    return 'Update Products Info'
#
#@app.route('/products/<int:prodcode>', methods=['DELETE'])
#def DeleteProductsInfo(prodcode):
#    return 'Delete Products Info'


if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0')
