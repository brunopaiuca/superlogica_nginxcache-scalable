import pymysql


class MySql:
    """ Mysql
    """
    host = "mysql"
    user = "root"
    pwd = "pass"
    db = "SUPERLOGICA_DEVOPS"
    MySQLError = pymysql.MySQLError
    ProgrammingError = pymysql.ProgrammingError

    def __init__(self):
        self.conn = pymysql.connect(
            host=self.host,
            port=3306,
            user=self.user,
            passwd=self.pwd,
            db=self.db,
            cursorclass=pymysql.cursors.DictCursor)

    def open(self):
        """ open
        """
        return self.conn.cursor()

    def close(self):
        """ close
        """
        return self.conn.close()

    def commit(self):
        """ commit
        """
        return self.conn.commit()
