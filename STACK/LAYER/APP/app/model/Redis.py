import redis


class Redis:
    """ Redis
    """
    host = "redis"
    port = 6379

    def __init__(self):
        self.conn = redis.Redis(
            host=self.host,
            port=self.port)

    def open(self):
        """ open
        """
        return self.conn
