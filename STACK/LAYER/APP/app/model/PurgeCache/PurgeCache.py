# coding=utf-8
from model.MySql import MySql
from model.Redis import Redis
from model.Product.Filter import Filter
import hashlib

class purgeCache:

    def __init__(self):
        self.redis = Redis()
        self.rediscur = self.redis.open()


    def mapURIs(self, product_id, NEW_product_price, OLD_product_price):

        filter = Filter()

        NEW_filterid = filter.CheckFilterIdByRange(NEW_product_price)

        if int(OLD_product_price) > 0:
            OLD_filterid = filter.CheckFilterIdByRange(OLD_product_price)
        else:
            OLD_filterid = 0

        MainURI      = "/products"
        ProdInfoURI  = "/products/" + str(product_id)
        NewFilterURI = "/products/filter/" + str(NEW_filterid)
        OldFilterURI = "/products/filter/" + str(OLD_filterid)

        MainURI      = MainURI.encode('utf-8')
        ProdInfoURI  = ProdInfoURI.encode('utf-8')
        NewFilterURI = NewFilterURI.encode('utf-8')
        OldFilterURI = OldFilterURI.encode('utf-8')

        MD5MainURI      = hashlib.md5( MainURI ).hexdigest()
        MD5ProdInfoURI  = hashlib.md5( ProdInfoURI ).hexdigest()
        MD5NewFilterURI = hashlib.md5( NewFilterURI ).hexdigest()
        MD5OldFilterURI = hashlib.md5( OldFilterURI ).hexdigest()

        return { "MainURI": MD5MainURI, "ProdInfoURI": MD5ProdInfoURI, "NewFilterURI": MD5NewFilterURI, "OldFilterURI": MD5OldFilterURI }


    def purgeCache(self, URIS):
 
       MainURI = URIS['MainURI']
       ProdInfoURI = URIS['ProdInfoURI'] 
       NewFilterURI = URIS['NewFilterURI']
       OldFilterURI = URIS['OldFilterURI'] or 0

       self.rediscur.set(MainURI, "1")
       self.rediscur.expire(MainURI, "30")

       self.rediscur.set(ProdInfoURI, "1")
       self.rediscur.expire(ProdInfoURI, "30")

       self.rediscur.set(NewFilterURI, "1")
       self.rediscur.expire(NewFilterURI, "30")

       self.rediscur.set(OldFilterURI, "1")
       self.rediscur.expire(OldFilterURI, "30")


    def destroy(self):
        self.rediscur.close()
        self.redis.close()
