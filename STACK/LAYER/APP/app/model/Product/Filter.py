# coding=utf-8
from model.MySql import MySql

class Filter:

    def __init__(self):
        self.mysqlr = MySql()
        self.curr = self.mysqlr.open()


    def CheckRangeByFilterid(self, filterid):

        start_window = 0
        limit_window = 0

        # Until 2k
        if filterid == 1:
            start_window = "0"
            limit_window = "2000"

        # Start 2001 until 4k
        elif filterid == 2:
            start_window = "2000"
            limit_window = "4000"

        # Greater Than 4k
        elif filterid == 3:
            start_window = "4000"
            limit_window = "99999999"

        return { "Start_Window": start_window, "Limit_Window": limit_window }


    def CheckFilterIdByRange(self, product_price):
        filterid = 0 
        self.curr.execute(""" SELECT F.ID as ID FROM FILTER as F WHERE %s >= F.START AND %s <= F.FINISH """,(product_price,product_price))
        filterid = self.curr.fetchone()['ID']
        return filterid


    def destroy(self):
        self.curr.close()
        self.mysqlr.close()
