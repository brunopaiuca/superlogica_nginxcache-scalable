# coding=utf-8
from model.MySql import MySql
from model.Product.Filter import Filter 
from model.PurgeCache.PurgeCache import purgeCache

class Product:
    """ Product Class
    """

    def __init__(self):
        import time
        time.sleep(3)
        self.mysql = MySql()
        self.cur = self.mysql.open()


#################################################
#################################################


    def getProductList(self):
        """
        get
        ---
        tags:
            - ProductList
        """

        self.cur.execute(""" SELECT P.ID, P.NAME, P.PRICE FROM PRODUCTS as P """)
        ret = []

        for row in self.cur:
            ret.append(  { 'Product_Id': row['ID'], 'Product_Name': row['NAME'], 'Product_Price': str(row['PRICE']) }  )

        return { 'products': ret }


#################################################
#################################################
    def createProduct(self, NEW_product_name, NEW_product_price):
        """
        post
        ---
        tags:
            - Product
        """

        try:
            self.cur.execute(""" SELECT COUNT(P.ID) as COUNTPROD FROM PRODUCTS as P WHERE P.NAME = %s""",(NEW_product_name))
            COUNT_PROD = self.cur.fetchone()['COUNTPROD']
            
            if COUNT_PROD == 0:
                self.cur.execute(""" INSERT INTO PRODUCTS (NAME, PRICE) VALUES (%s, %s)""",(NEW_product_name,NEW_product_price))
                self.mysql.commit()
                product_id = self.cur.lastrowid

                purgecache = purgeCache()
                purgecache_uris = purgecache.mapURIs(product_id, NEW_product_price,"0")

                purgecache.purgeCache(purgecache_uris)

                return { "success": True, "Product_Id": product_id }
            else:
                return { "success": False, "msg": "Product already in DB" }

        except Exception as e:
            return { "success": False, "msg": str(e) }


#################################################
#################################################


    def getProductListByFilter(self, filterid):
        """
        get
        ---
        tags:
            - ProductListByFilter
        """

        filter = Filter()
        jsonCheckFilter = filter.CheckRangeByFilterid(filterid)

        start_window = jsonCheckFilter['Start_Window']
        limit_window = jsonCheckFilter['Limit_Window'] 
        
        self.cur.execute(""" SELECT P.ID, P.NAME, P.PRICE FROM PRODUCTS as P WHERE P.PRICE Between %s And %s""",(start_window,limit_window))
        ret = []

        for row in self.cur:
            ret.append(  { 'Product_Id': row['ID'], 'Product_Name': row['NAME'], 'Product_Price': str(row['PRICE']) }  )

        return { 'products': ret }


#################################################
#################################################

    def getProductInfo(self, product_id):
        """
        get
        ---
        tags:
            - ProductInfo
        """

        self.cur.execute(""" SELECT P.ID, P.NAME, P.PRICE FROM PRODUCTS as P WHERE P.ID = %s""",(product_id))

        ret = []

        for row in self.cur:
            ret.append(  { 'Product_Id': row['ID'], 'Product_Name': row['NAME'], 'Product_Price': str(row['PRICE']) }  )

        return { 'products': ret }


#################################################
#################################################

    def updateProductInfo(self, product_id, NEW_product_name, NEW_product_price):
        """
        put
        ---
        tags:
            - ProductInfo
        """

        try:
            self.cur.execute(""" SELECT P.PRICE FROM PRODUCTS as P WHERE P.ID = %s""",(product_id))

            OLD_product_price = self.cur.fetchone()['PRICE']

            purgecache = purgeCache()
            purgecache_uris = purgecache.mapURIs(product_id, NEW_product_price, OLD_product_price)

            self.cur.execute(""" UPDATE PRODUCTS as P SET P.NAME = %s, P.PRICE = %s WHERE P.ID = %s""",(NEW_product_name,NEW_product_price,product_id))
            self.mysql.commit()

            purgecache.purgeCache(purgecache_uris)

            return { "success": True}

        except:
            return { "success": False}


#################################################
#################################################

    def destroy(self):
        self.cur.close()
        self.mysql.close()
